using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Entities
{
    public class Item
    {
        [Key()]
        public int Id { get; set; }
        public string ItemVendido { get; set; }
    }
}