using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        [Key()]
        public int Id { get; set; }
        [ForeignKey("Vendedor")]
        public int DadosVendedor { get; set; }
        public virtual Vendedor Vendedor { get; set; }
        [ForeignKey("Item")]
        public DateTime Data { get; set; }
        public int IdItem { get; set; }
        public virtual Item ItemVenda{ get; set; }
        public EnumStatusVenda Status { get; set; }
        
    }
}