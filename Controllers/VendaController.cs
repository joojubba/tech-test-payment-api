using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly OperacoesContext _context;
        public VendaController(OperacoesContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult CriarVenda(Venda venda)
        {
            venda.Status = EnumStatusVenda.AguadandoPagamento;
            venda.Data = DateTime.Now;

            _context.Add(venda);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterVendaPorId), new { id = venda.Id }, venda);
        }

        [HttpGet("{id}")]
        public  IActionResult ObterVendaPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            var Vendedor = _context.Vendedores.Find(venda.DadosVendedor);
            var item = _context.Itens.Find(venda.IdItem);

             if(venda == null)
            {
                return NotFound();
            }
            return Ok(venda);
        }

        [HttpGet("ObterTodasVendas")]
        public IActionResult ObterTodasVendas()
        {
            var venda = _context.Vendas;
            return Ok(venda);
        }

        [HttpGet("ObterPorStatus")]
        public IActionResult ObterPorStatus(EnumStatusVenda status)
        {
            var venda = _context.Vendas.Where(x => x.Status == status);
            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarVenda(int id, Venda venda)
        {
            var vendaBanco = _context.Vendas.Find(venda.Id);

            if (vendaBanco == null)
                return NotFound();

            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            return Ok(vendaBanco);
        }
        
        [HttpDelete("{id}")]
        public IActionResult DeletarVenda(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            _context.Vendas.Remove(vendaBanco);
            _context.SaveChanges();

            return NoContent();
        }
    }
}