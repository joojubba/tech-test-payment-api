using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItemController : ControllerBase
    {
        private readonly OperacoesContext _context;
        public ItemController(OperacoesContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult CriarItem(Item item)
        {
            _context.Add(item);
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterItemPorId), new { id = item.Id }, item);
        }

        [HttpGet("{id}")]
        public IActionResult ObterItemPorId(int id)
        {
            var item = _context.Itens.Find(id);
            if(item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }

        [HttpGet("ObterTodosItens")]
        public IActionResult ObterTodosItens()
        {
            var item = _context.Itens;
            return Ok(item);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarItem(int id, Item item)
        {
            var itemBanco = _context.Itens.Find(id);

            if (itemBanco == null)
                return NotFound();

            itemBanco.ItemVendido = item.ItemVendido; 

            _context.Itens.Update(itemBanco);
            _context.SaveChanges();

            return Ok(itemBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult DeletarItem(int id)
        {
            var itemBanco = _context.Itens.Find(id);

            if (itemBanco == null)
                return NotFound();

            _context.Itens.Remove(itemBanco);
            _context.SaveChanges();

            return NoContent();
        }       
    }
}