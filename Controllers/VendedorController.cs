using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly OperacoesContext _context;
        public VendedorController(OperacoesContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult CriarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();          
            return CreatedAtAction(nameof(ObterVendedorPorId), new { id = vendedor.Id }, vendedor);
        }

        [HttpGet("{id}")]
        public IActionResult ObterVendedorPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if(vendedor == null)
            {
                return NotFound();
            }
            return Ok(vendedor);
        }

        [HttpGet("ObterTodosVendedores")]
        public IActionResult ObterTodos()
        {
            var vendedor = _context.Vendedores;
            return Ok(vendedor);
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarVendedor(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
                return NotFound();

            vendedorBanco.Nome = vendedor.Nome; 
            vendedorBanco.CPF = vendedor.CPF;
            vendedorBanco.Email = vendedor.Email;
            vendedorBanco.Telefone = vendedor.Telefone;

            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();
            return Ok(vendedorBanco);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
                return NotFound();

            _context.Vendedores.Remove(vendedorBanco);
            _context.SaveChanges();
            return NoContent();
        }
    }
    
  
}