using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Entities;

//verificar esse using entities

namespace tech_test_payment_api.Context
{
    public class OperacoesContext : DbContext
    {
        public OperacoesContext(DbContextOptions<OperacoesContext> options) : base(options)
        {
            
        }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Item> Itens { get; set; }
    }
}